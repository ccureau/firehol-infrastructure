# resource "aws_security_group" "ingress-ssh-test" {
#   name   = "allow-ssh-sg"
#   vpc_id = module.vpc.vpc_id

#   ingress {
#     cidr_blocks = [
#       "0.0.0.0/0"
#     ]

#     from_port = 22
#     to_port   = 22
#     protocol  = "tcp"
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# resource "aws_key_pair" "spot_key" {
#   key_name   = "spot_key"
#   public_key = file("/home/ccureau/.ssh/id_rsa.pub")
# }

# resource "aws_network_interface" "spot_nic1" {
#   subnet_id = sort(data.aws_subnet_ids.private_us_west_2a.ids)[0]
# }

# resource "aws_network_interface" "spot_nic2" {
#   subnet_id       = sort(data.aws_subnet_ids.public_us_west_2a.ids)[0]
#   security_groups = [aws_security_group.ingress-ssh-test.id]
# }

# resource "aws_eip" "spot_eip" {
#   network_interface = aws_network_interface.spot_nic2.id
#   vpc               = true
# }

# resource "aws_spot_instance_request" "test_instance" {
#   ami                    = data.aws_ami.amazon_linux_2.id
#   spot_price             = "0.2"
#   instance_type          = "t3.medium"
#   spot_type              = "one-time"
#   block_duration_minutes = "360"
#   wait_for_fulfillment   = "true"
#   key_name               = "spot_key"

#   network_interface {
#     device_index         = 0
#     network_interface_id = aws_network_interface.spot_nic1.id
#   }

#   network_interface {
#     device_index         = 1
#     network_interface_id = aws_network_interface.spot_nic2.id
#   }
# }
