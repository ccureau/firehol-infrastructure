variable "aws_region" {
  description = "The AWS region to perform actions in"
  default     = "us-west-2"
  type        = string
}

variable "aws_profile" {
  description = "The AWS profile to use when performing actions"
  default     = "default"
  type        = string
}

variable "name" {
  description = "Descriptive name for this project"
  type        = string
}

variable "vpc_cidr_block" {
  description = "The CIDR block to assign to this VPC"
  type        = string
}

variable "vpc_private_subnets" {
  description = "The CIDR block to create private subnets"
  type        = list(string)
}

variable "vpc_public_subnets" {
  description = "The CIDR block to create public subnets"
  type        = list(string)
}

variable "docdb_database_name" {
  description = "docdb database name"
  type        = string
}

variable "docdb_instance_class" {
  description = "docdb instance type (e.g., db.t3.medium)"
  type        = string
  default     = "db.t3.medium"
}

variable "docdb_username" {
  description = "docdb master username"
  type        = string
}

variable "docdb_password" {
  description = "docdb master password"
  type        = string
}

variable "docdb_cache_collection_name" {
  description = "docdb cache collection name"
  type        = string
}

variable "docdb_list_collection_name" {
  description = "docdb cache collection name"
  type        = string
}

variable "docdb_override_collection_name" {
  description = "docdb override collection name"
  type        = string
}

variable "docdb_cache_expiration_seconds" {
  description = "docdb cache expiration in seconds"
  type        = string
}

variable "aws_access_key_id" {
  description = "aws access key id"
  type        = string
}

variable "aws_secret_access_key" {
  description = "aws secret access key"
  type        = string
}

variable "cloudwatch_alarm_email" {
  description = "email address to send alarms to"
  type        = list(string)
}
