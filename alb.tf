module "alb" {
  source = "terraform-aws-modules/alb/aws"

  name            = "${var.name}-alb"
  vpc_id          = module.vpc.vpc_id
  subnets         = [module.vpc.public_subnets[0], module.vpc.public_subnets[1]]
  security_groups = [aws_security_group.load_balancer.id, aws_security_group.ecs_task.id]

  target_groups = [
    {
      name_prefix      = "blue-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "ip"
      health_check = {
        enabled             = true
        healthy_threshold   = 5
        interval            = 30
        matcher             = "200"
        path                = "/health"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = 5
        unhealthy_threshold = 2
      }
    },
    {
      name_prefix      = "green-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "ip"
      health_check = {
        enabled             = true
        healthy_threshold   = 5
        interval            = 30
        matcher             = "200"
        path                = "/health"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = 5
        unhealthy_threshold = 2
      }
    }
  ]

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

  https_listeners = [
    {
      # certificate_arn    = data.aws_acm_certificate.wildcard_cureau_net.arn
      certificate_arn    = aws_acm_certificate_validation.fluentstream_cureau_net.certificate_arn
      protocol           = "HTTPS"
      port               = 443
      target_group_index = 0
    }
  ]
}
