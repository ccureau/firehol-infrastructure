resource "aws_security_group" "load_balancer" {
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group" "ecs_task" {
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "alb_ingress_http" {
  type      = "ingress"
  from_port = 80
  to_port   = 80
  protocol  = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  security_group_id = aws_security_group.load_balancer.id
}

resource "aws_security_group_rule" "alb_ingress_https" {
  type      = "ingress"
  from_port = 443
  to_port   = 443
  protocol  = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  security_group_id = aws_security_group.load_balancer.id
}

resource "aws_security_group_rule" "ecs_ingress_ssh" {
  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  security_group_id = aws_security_group.ecs_task.id
}

resource "aws_security_group_rule" "ecs_ingress_http" {
  type                     = "ingress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ecs_task.id
  source_security_group_id = aws_security_group.load_balancer.id
}

resource "aws_security_group_rule" "alb_egress" {
  type      = "egress"
  from_port = 0
  to_port   = 65535
  protocol  = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  security_group_id = aws_security_group.load_balancer.id
}

resource "aws_security_group_rule" "ecs_egress" {
  type      = "egress"
  from_port = 0
  to_port   = 65535
  protocol  = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
  security_group_id = aws_security_group.ecs_task.id
}
