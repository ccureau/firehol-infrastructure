resource "aws_ecs_cluster" "cluster" {
  name = "${var.name}-cluster"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

# Resources below are for EC2 backed ECS cluster

# resource "aws_key_pair" "ecs_key" {
#   key_name   = "spot_key"
#   public_key = file("/home/ccureau/.ssh/id_rsa.pub")
# }

# resource "aws_launch_configuration" "ecs_launch_config" {
#   iam_instance_profile = aws_iam_instance_profile.ecs_agent.name
#   security_groups      = [aws_security_group.ecs_task.id]
#   instance_type        = "t3a.medium"
#   image_id             = data.aws_ami.amazon_ecs.id
#   # key_name             = aws_key_pair.ecs_key.key_name
#   user_data = <<EOF
# #!/bin/bash
# echo ECS_CLUSTER=${aws_ecs_cluster.cluster.name} >>/etc/ecs/ecs.config
# echo ECS_ENABLE_SPOT_INSTANCE_DRAINING=true >>/etc/ecs/ecs.config
# EOF
# }

# resource "aws_autoscaling_group" "ecs_asg" {
#   name                 = "${var.name}-ecs-asg"
#   vpc_zone_identifier  = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
#   launch_configuration = aws_launch_configuration.ecs_launch_config.name

#   desired_capacity          = 1
#   min_size                  = 1
#   max_size                  = 2
#   health_check_grace_period = 300
#   health_check_type         = "EC2"
# }
