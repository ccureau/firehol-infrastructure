resource "aws_route53_zone" "fluentstream_cureau_net" {
  name = "fluentstream.cureau.net"

  lifecycle {
    prevent_destroy = true
  }
}
resource "aws_route53_record" "firehol_checker" {
  zone_id = aws_route53_zone.fluentstream_cureau_net.zone_id
  name    = "firehol-checker.fluentstream.cureau.net"
  type    = "CNAME"
  ttl     = "300"
  records = [module.alb.lb_dns_name]

  depends_on = [
    module.alb
  ]
}

resource "aws_acm_certificate" "fluentstream_cureau_net" {
  domain_name       = "*.fluentstream.cureau.net"
  validation_method = "DNS"
}

resource "aws_route53_record" "fluentstream_cureau_net_validation" {
  for_each = {
    for dvo in aws_acm_certificate.fluentstream_cureau_net.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.fluentstream_cureau_net.zone_id
}

resource "aws_acm_certificate_validation" "fluentstream_cureau_net" {
  certificate_arn         = aws_acm_certificate.fluentstream_cureau_net.arn
  validation_record_fqdns = [for record in aws_route53_record.fluentstream_cureau_net_validation : record.fqdn]
}
