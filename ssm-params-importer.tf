resource "aws_ssm_parameter" "importer-git-repository" {
  name  = "/config/firehol-importer/git/repository"
  type  = "String"
  value = "https://github.com/firehol/blocklist-ipsets.git"
}

resource "aws_ssm_parameter" "importer-docdb-url" {
  name      = "/config/firehol-importer/docdb/url"
  type      = "String"
  value     = aws_docdb_cluster.service.endpoint
  overwrite = "true"
}

resource "aws_ssm_parameter" "importer-docdb-databaseName" {
  name  = "/config/firehol-importer/docdb/databaseName"
  type  = "String"
  value = var.docdb_database_name
}

resource "aws_ssm_parameter" "importer-docdb-username" {
  name  = "/config/firehol-importer/docdb/username"
  type  = "String"
  value = var.docdb_username
}

resource "aws_ssm_parameter" "importer-docdb-password" {
  name  = "/config/firehol-importer/docdb/password"
  type  = "String"
  value = var.docdb_password
}

resource "aws_ssm_parameter" "importer-docdb-cacheCollectionName" {
  name  = "/config/firehol-importer/docdb/cacheCollectionName"
  type  = "String"
  value = var.docdb_cache_collection_name
}

resource "aws_ssm_parameter" "importer-docdb-listCollectionName" {
  name  = "/config/firehol-importer/docdb/listCollectionName"
  type  = "String"
  value = var.docdb_list_collection_name
}

resource "aws_ssm_parameter" "importer-docdb-overrideCollectionName" {
  name  = "/config/firehol-importer/docdb/overrideCollectionName"
  type  = "String"
  value = var.docdb_override_collection_name
}

resource "aws_ssm_parameter" "importer-docdb-cacheExpireTime" {
  name  = "/config/firehol-importer/docdb/cacheExpireTime"
  type  = "String"
  value = var.docdb_cache_expiration_seconds
}
