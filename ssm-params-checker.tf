resource "aws_ssm_parameter" "firehol-docdb-url" {
  name      = "/config/firehol-checker/docdb/url"
  type      = "String"
  value     = aws_docdb_cluster.service.endpoint
  overwrite = "true"
}

resource "aws_ssm_parameter" "firehol-docdb-databaseName" {
  name  = "/config/firehol-checker/docdb/databaseName"
  type  = "String"
  value = var.docdb_database_name
}

resource "aws_ssm_parameter" "firehol-docdb-username" {
  name  = "/config/firehol-checker/docdb/username"
  type  = "String"
  value = var.docdb_username
}

resource "aws_ssm_parameter" "firehol-docdb-password" {
  name  = "/config/firehol-checker/docdb/password"
  type  = "String"
  value = var.docdb_password
}

resource "aws_ssm_parameter" "firehol-docdb-cacheCollectionName" {
  name  = "/config/firehol-checker/docdb/cacheCollectionName"
  type  = "String"
  value = var.docdb_cache_collection_name
}

resource "aws_ssm_parameter" "firehol-docdb-listCollectionName" {
  name  = "/config/firehol-checker/docdb/listCollectionName"
  type  = "String"
  value = var.docdb_list_collection_name
}

resource "aws_ssm_parameter" "firehol-docdb-overrideCollectionName" {
  name  = "/config/firehol-checker/docdb/overrideCollectionName"
  type  = "String"
  value = var.docdb_override_collection_name
}

resource "aws_ssm_parameter" "firehol-docdb-cacheExpireTime" {
  name  = "/config/firehol-checker/docdb/cacheExpireTime"
  type  = "String"
  value = var.docdb_cache_expiration_seconds
}
