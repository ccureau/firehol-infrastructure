data "aws_subnet_ids" "private_us_west_2a" {
  vpc_id = module.vpc.vpc_id
  filter {
    name   = "tag:Name"
    values = ["tf-${var.name}-private-us-west-2a"]
  }

  depends_on = [module.vpc]
}

data "aws_subnet_ids" "public_us_west_2a" {
  vpc_id = module.vpc.vpc_id
  filter {
    name   = "tag:Name"
    values = ["tf-${var.name}-public-us-west-2a"]
  }

  depends_on = [module.vpc]
}

data "aws_iam_policy_document" "ecs_task_execution_role" {
  version = "2012-10-17"
  statement {
    sid     = ""
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

data "aws_ami" "amazon_ecs" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-*"]
  }
}

data "aws_acm_certificate" "wildcard_cureau_net" {
  domain   = "*.cureau.net"
  statuses = ["ISSUED"]
}
