terraform {
  backend "s3" {
    bucket = "fluentstream-tf-state"
    key    = "firehol-project/terraform.tfstate"
    region = "us-west-2"
  }
}
