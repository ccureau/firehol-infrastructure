resource "aws_security_group" "service" {
  name   = "${var.name}-docdb-sg"
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = concat(var.vpc_private_subnets, var.vpc_public_subnets)
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_docdb_subnet_group" "service" {
  name       = "${var.name}-docdb-subnet-groups"
  subnet_ids = module.vpc.public_subnets
}

resource "aws_docdb_cluster_instance" "service" {
  count              = 1
  identifier         = "${var.name}-cluster-instance-${count.index}"
  cluster_identifier = aws_docdb_cluster.service.id
  instance_class     = var.docdb_instance_class
}

resource "aws_docdb_cluster" "service" {
  skip_final_snapshot             = true
  db_subnet_group_name            = aws_docdb_subnet_group.service.name
  cluster_identifier              = "${var.name}-docdb-cluster"
  engine                          = "docdb"
  master_username                 = var.docdb_username
  master_password                 = var.docdb_password
  db_cluster_parameter_group_name = aws_docdb_cluster_parameter_group.service.name
  vpc_security_group_ids          = [aws_security_group.service.id]
}

resource "aws_docdb_cluster_parameter_group" "service" {
  family = "docdb4.0"
  name   = "tf-${var.name}"

  parameter {
    name  = "tls"
    value = "disabled"
  }
}
