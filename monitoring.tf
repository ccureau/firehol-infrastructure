resource "aws_sns_topic" "ecs_container_state_alert" {
  name = "${var.name}-ecs-container-state-alert"
}

resource "aws_sns_topic_subscription" "ecs_container_state_alert_email" {
  for_each  = toset(var.cloudwatch_alarm_email)
  topic_arn = aws_sns_topic.ecs_container_state_alert.arn
  protocol  = "email"
  endpoint  = each.key
}

resource "aws_cloudwatch_event_rule" "ecs_container_state" {
  name        = "${var.name}-ecs-container-state"
  description = "Monitor container states"

  event_pattern = <<EOF
{
    "source": [
        "aws.ecs"
    ],
    "detail-type": [
        "ECS Task State Change",
        "ECS Container Instance State Change"
    ],
    "detail": {
        "clusterArn": [
            "${aws_ecs_cluster.cluster.arn}"
    ],
    "lastStatus": [
        "STOPPED"
    ]
    }
}
EOF
}

resource "aws_cloudwatch_event_target" "ecs_container_state" {
  target_id = "ecs_container_state"
  arn       = aws_sns_topic.ecs_container_state_alert.arn
  rule      = aws_cloudwatch_event_rule.ecs_container_state.name

  input_transformer {
    input_paths = {
      stoppedReason = "$.detail.stoppedReason",
      lastStatus    = "$.detail.lastStatus",
      group         = "$.detail.group",
    }
    input_template = "\"The container <group> status is <lastStatus> - cause: <stoppedReason>\""
  }
}
