module "importer_container" {
  source = "./modules/terraform-aws-ecs-container-definition"

  container_name           = "firehol-importer"
  container_image          = "${aws_ecr_repository.firehol_importer.repository_url}:latest"
  container_memory         = 3584
  readonly_root_filesystem = false
  essential                = true
  privileged               = false

  log_configuration = {
    logDriver = "awslogs"
    options = {
      awslogs-region        = var.aws_region
      awslogs-group         = aws_cloudwatch_log_group.firehol_importer_cloudwatch.name
      awslogs-stream-prefix = "ecs"
    }
    secretOptions = null
  }

  ulimits = [
    {
      name      = "nofile"
      softLimit = 65536
      hardLimit = 65536
    }
  ]
  port_mappings = [
    {
      containerPort = 8080
      hostPort      = 8080
      protocol      = "tcp"
    }
  ]

  environment = [
    {
      name  = "MICRONAUT_ENVIRONMENTS"
      value = "ec2"
    },
    {
      name  = "AWS_ACCESS_KEY_ID"
      value = var.aws_access_key_id
    },
    {
      name  = "AWS_SECRET_ACCESS_KEY"
      value = var.aws_secret_access_key
    },
    {
      name  = "PORT"
      value = "8080"
    }
  ]
}

resource "aws_cloudwatch_log_group" "firehol_importer_cloudwatch" {
  name              = "${var.name}-importer"
  retention_in_days = 7
}

resource "aws_ecs_task_definition" "firehol_importer_service" {
  family                   = "firehol-importer"
  container_definitions    = module.importer_container.json_map_encoded_list
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_task_role.arn
  cpu                      = 2048
  memory                   = 4096
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  depends_on               = [aws_docdb_cluster_instance.service]
}

resource "aws_ecs_service" "firehol_importer" {
  name                               = "firehol-importer"
  cluster                            = aws_ecs_cluster.cluster.id
  task_definition                    = aws_ecs_task_definition.firehol_importer_service.arn
  desired_count                      = 1
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"
  

  network_configuration {
    security_groups  = [aws_security_group.ecs_task.id]
    subnets          = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
    assign_public_ip = false
  }

  # placement_constraints {
  #   type       = "memberOf"
  #   expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  # }

  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }

  depends_on = [aws_iam_role_policy_attachment.ecs-task-execution-role-policy-attachment, module.vpc]
}
